### 架构说明
1、SpringBoot应用集成Micrometer向Prometheus上报Metrics数据。<br/>
2、Prometheus服务端配置SpringBoot应用Target。<br/>
3、Grafana配置数据源为Prometheus的仪表板，监控SpringBoot应用。<br/>

### 应用集成Micrometer
1、引入相应依赖：spring-boot-starter-actuator、micrometer-registry-prometheus、micrometer-registry-jmx、micrometer-jvm-extras

```
implementation 'org.springframework.boot:spring-boot-starter-actuator'
implementation 'io.micrometer:micrometer-registry-prometheus:1.2.0'
implementation 'io.micrometer:micrometer-registry-jmx:1.2.0'
implementation 'io.github.mweirauch:micrometer-jvm-extras:0.1.4'
```

2、配置自定义MeterRegistry

```
/**
 * @Project Name:mmp
 * @Package Name:cn.hoofungson.mmp.config
 * @Since JDK 1.8
 */

package cn.hoofungson.mmp.config;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: WebConfig
 * @Author 胡松 hoofungson@163.com
 * @Date 2020-04-16 13:18
 * @Version V1.0
 */
@Configuration
public class WebConfig {

    /**
     * 配置自定义MeterRegistry
     **/
    @Bean
    MeterRegistryCustomizer<MeterRegistry> configurer(@Value("${spring.application.name}") String applicationName) {
        return registry -> registry.config().commonTags("application", applicationName);
    }
}
```

3、配置Properties暴露端点

```
spring.application.name=message-management-platform
management.endpoints.web.exposure.include=*
management.metrics.tags.application=${spring.application.name}
```

### 验证集成Micrometer
1、访问URL http://<IP地址>:<端口>/actuator/metrics和http://<IP地址>:<端口>/actuator/prometheus，若有返回数据则集成成功。

```
curl http://localhost:8061/actuator/metrics
curl http://localhost:8061/actuator/prometheus
```